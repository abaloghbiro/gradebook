package hu.braininghub.bh06.gradebook;

import java.util.ArrayList;
import java.util.List;

public class Gradebook {

	private List<GradebookEntry> entries = new ArrayList<GradebookEntry>();

	public List<GradebookEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<GradebookEntry> entries) {
		this.entries = entries;
	}
	
	
	
}
