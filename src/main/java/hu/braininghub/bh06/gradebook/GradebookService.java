package hu.braininghub.bh06.gradebook;

import java.util.List;

public interface GradebookService {

	List<GradebookEntry> getGradebookEntriesByStudentAndSubjectName(String studentName,String subjectName);
}
