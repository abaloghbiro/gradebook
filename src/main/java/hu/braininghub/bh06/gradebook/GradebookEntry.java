package hu.braininghub.bh06.gradebook;

import java.util.List;
import java.util.Map;

public class GradebookEntry {

	private Student student;
	private Map<Subject, List<Integer>> entries;
	
	
	public GradebookEntry(Student st) {
		this.student = st;
	}
	
	public Student getStudent() {
		return student;
	}
	
	public Map<Subject, List<Integer>> getEntries() {
		return entries;
	}
	public void setEntries(Map<Subject, List<Integer>> entries) {
		this.entries = entries;
	}
	
	
}
