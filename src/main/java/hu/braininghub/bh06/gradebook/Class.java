package hu.braininghub.bh06.gradebook;

import java.util.List;

public class Class {

	private String name;
	private Teacher headTeacher;
	private Gradebook gradebook;
	private List<Student> students;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Teacher getHeadTeacher() {
		return headTeacher;
	}

	public void setHeadTeacher(Teacher headTeacher) {
		this.headTeacher = headTeacher;
	}

	public Gradebook getGradebook() {
		return gradebook;
	}

	public void setGradebook(Gradebook gradebook) {
		this.gradebook = gradebook;
	}

	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

}
