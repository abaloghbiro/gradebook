package hu.braininghub.bh06.gradebook;

public class Subject {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
